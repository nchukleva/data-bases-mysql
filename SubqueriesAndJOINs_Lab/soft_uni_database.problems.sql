#Lab_01_Managers
SELECT e.`employee_id`, CONCAT(e.`first_name`, ' ', e.`last_name`) AS `full_name`, d.`department_id`, d.`name`
FROM `employees` AS e
JOIN `departments` AS d
ON e.`employee_id` = d.`manager_id`
ORDER BY e.`employee_id`
LIMIT 5;


#Lab_02_Town Addresses
SELECT t.`town_id`, t.`name`, a.`address_text`
FROM `towns` AS t
JOIN `addresses` AS a
ON t.`town_id` = a.`town_id`
WHERE t.`name` IN ('San Francisco', 'Sofia', 'Carnation')
ORDER BY t.`town_id`, a.`address_id`;


#Lab_03_Employees Without Managers
SELECT `employee_id`, `first_name`, `last_name`, `department_id`, `salary`
FROM `employees`  
WHERE `manager_id` IS NULL;


#Lab_04_Higher Salary
SELECT COUNT(e.`employee_id`) AS `count` 
FROM `employees` AS e
WHERE e.`salary` > 
(
	SELECT AVG(`salary`) 
    FROM employees
);

