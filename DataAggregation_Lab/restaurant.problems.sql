SELECT SUM(`salary`) AS `total_sum`
FROM `soft_uni`.`employees`
GROUP BY `department_id`;

SELECT SUM(`salary`) AS `total_sum`
FROM `restaurant`.`employees`
GROUP BY `department_id`;

SELECT COUNT(*), `department_id` 
FROM `employees`
GROUP BY `department_id`
ORDER BY `department_id`;

#Lab_1_Departments Info
SELECT `department_id`, COUNT(*) AS `Number of employees` 
FROM `employees`
GROUP BY `department_id`
ORDER BY `department_id`;


#Lab_2_Average Salary
SELECT `department_id`, ROUND(AVG(`salary`), 2) AS `Average Salary` 
FROM `employees`
GROUP BY `department_id`
ORDER BY `department_id`;


#Lab_3_Min Salary
SELECT `department_id`, ROUND(MIN(`salary`),2) AS `Min Salary`
FROM `employees`
GROUP BY `department_id`
HAVING `Min Salary` > 800;


#Lab_4_Appetizers Count
SELECT COUNT(*)
FROM `products`
WHERE `category id` = 2 AND `price` > 8;


#Lab_5_Menu Prices
SELECT `category_id`, ROUND(AVG(`price`), 2) AS `Average Price`, 
	ROUND(MIN(`price`), 2) AS `Cheapest Product`,
    ROUND(MAX(`price`), 2) AS `Most Expensive Product`
FROM `products`
GROUP BY `category_id`;
