#MANY-TO-MANY references
CREATE TABLE employees (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `employee_name` VARCHAR(50)
);

CREATE TABLE projects (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`project_name` VARCHAR(50)
);

CREATE TABLE employees_projects (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`employee_id` INT,
`project_id` INT,
CONSTRAINT fk_employees_projects_employees
FOREIGN KEY(`employee_id`) REFERENCES `employees`(`id`),
CONSTRAINT fk_employees_projects_projects
FOREIGN KEY(`project_id`) REFERENCES `projects`(`id`)
);



#Lab_01_Mountains and Peaks
CREATE TABLE `mountains` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(50)
);

CREATE TABLE `peaks` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(50),
`mountain_id` INT,
CONSTRAINT fk_peaks_mountains
FOREIGN KEY (`mountain_id`) REFERENCES `mountains`(`id`)
);


#Lab_02_Trip Organization
SELECT c.id AS `driver_id`, v.vehicle_type, CONCAT(`first_name`, ' ', `last_name`)
FROM `campers` AS c
JOIN `vehicles` AS v
ON v.driver_id = c.id;


#Lab_03_SoftUni Hiking
SELECT 
	r.`starting_point` AS `route_starting_point`, 
    r.`end_point` AS `route_ending_point`, 
    r.`leader_id`, CONCAT(`first_name`, ' ', `last_name`) AS `leader_name`
FROM `routes` AS r
JOIN `campers` AS c
ON c.`id` = r.`leader_id`;


#Lab_04_Delete Mountains
DROP TABLE `peaks`;
DROP TABLE `mountains`;

CREATE TABLE `mountains` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(50)
);

CREATE TABLE `peaks` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(50),
`mountain_id` INT,
CONSTRAINT fk_peaks_mountains
FOREIGN KEY (`mountain_id`) REFERENCES `mountains`(`id`)
ON DELETE CASCADE
);