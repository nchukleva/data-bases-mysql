#Ex_12_Games from 2011 and 2012 Year
SELECT `name`, DATE_FORMAT(`start`, '%Y-%m-%d') 
FROM `games`
WHERE YEAR(`start`) IN (2011, 2012)
ORDER BY `start`
LIMIT 50;


#Ex_13_User Email Providers
SELECT * FROM `users`;
SELECT `user_name`, SUBSTRING(`email`, LOCATE('@', `email`) + 1) AS `email provider`
FROM `users`
ORDER BY `email provider`, `user_name`;


#Ex_14_Get Users with IP Address Like Pattern
SELECT `user_name`, `ip_address` FROM `users`
WHERE `ip_address` LIKE "___.1%.%.___" 
ORDER BY `user_name`;


#Ex_15_Show All Games with Duration and Part of the Day
SELECT 
	`name`,
	(CASE
		WHEN HOUR(`start`) BETWEEN 0 AND 11 THEN 'Morning'
		WHEN HOUR(`start`) BETWEEN 12 AND 17 THEN 'Afternoon'
		WHEN HOUR(`start`) BETWEEN 18 AND 23 THEN 'Evening'
        #ELSE 'Evening'
    END) AS 'Part of the Day',
	(CASE
		WHEN `duration` <= 3 THEN 'Extra Short'
		WHEN `duration` <= 6 THEN 'Short'
		WHEN `duration` <= 10 THEN 'Long'
		ELSE 'Extra Long'
	END) AS 'Duration'
FROM `games`;

