#Ex_0_Create Database
CREATE DATABASE `minions`;
USE `minions`;


#Ex_1_Create Tables
CREATE TABLE `minions` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(50),
`age` INT 
);

CREATE TABLE `towns` (
`town_id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR (20)
);


#Ex_2_Alter Minions Table
ALTER TABLE `minions`.`towns` 
CHANGE COLUMN `town_id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `minions`
ADD COLUMN `town_id` INT,
ADD CONSTRAINT fk_minions_towns
FOREIGN KEY (`town_id`) REFERENCES `towns`(`id`);


#Ex_3_Insert Records in Both Tables
INSERT INTO `towns`
VALUES
(1, 'Sofia'),
(2, 'Plovdiv'),
(3, 'Varna');

INSERT INTO `minions`
VALUES
(1, 'Kevin', 22, 1),
(2, 'Bob', 15, 3),
(3, 'Steward', NULL, 2);


#Ex_4_Truncate Table Minions
TRUNCATE `minions`;


#Ex_5_Drop All Tables
DROP TABLE `minions`;
DROP TABLE `towns`;


#Ex_6_Create Table People
CREATE TABLE `people` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(200) NOT NULL,
`picture` BLOB,
`height` DOUBLE,
`weight` DOUBLE,
`gender` CHAR(1) NOT NULL,
`birthdate` DATE NOT NULL,
`biography` TEXT
);

INSERT INTO `people`
VALUES
(1, 'Name1', NULL, 1.72, 65.9, 'M', '2000-12-12', 'Insert text here'),
(2, 'Name2', NULL, 1.81, 71, 'M', '1990-06-12', 'Insert text here'),
(3, 'Name3', NULL, 1.51, 43, 'F', '1980-06-23', 'Insert text here'),
(4, 'Name4', NULL, 1.22, 30, 'F', '2010-06-09', 'Insert text here'),
(5, 'Name5', NULL, 1.22, 30, 'F', '2010-06-09', 'Insert text here');


#Ex_7_Create Table Users
CREATE TABLE `users` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`username` VARCHAR(30) NOT NULL,
`password` VARCHAR(30) NOT NULL,
`profile_picture` BLOB,
`last_login_time` TIME,
`is_deleted` BOOLEAN
);

INSERT INTO `users`
VALUES
(1, 'Name1', 'name1', NULL, '12:12:12', false),
(2, 'Name2', 'name2', NULL, '12:32:45', true),
(3, 'Name3', 'name3', NULL, '13:01:45', true),
(4, 'Name4', 'name4', NULL, '13:16:50', true),
(5, 'Name5', 'name5', NULL, '13:33:12', false);


#Ex_8_Change Primary Key
ALTER TABLE `users` 
DROP PRIMARY KEY,
ADD PRIMARY KEY (`id`, `username`);


#Ex_9_Set Default Value of a Field
ALTER TABLE `users`
CHANGE COLUMN `last_login_time` `last_login_time` TIME DEFAULT NOW();


#Ex_10_Set Unique Field
ALTER TABLE `users` 
CHANGE COLUMN `username` `username` VARCHAR(30) NOT NULL UNIQUE,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`id`);


#Ex_11_Movies Database
CREATE DATABASE `movies`;
USE `movies`;

CREATE TABLE `directors` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`director_name` VARCHAR(30) NOT NULL,
`notes` TEXT
);

CREATE TABLE `genres` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`genre_name` VARCHAR(30) NOT NULL,
`notes` TEXT
);

CREATE TABLE `categories` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`category_name` VARCHAR(30) NOT NULL,
`notes` TEXT
);

/*CREATE TABLE `movies` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`title` VARCHAR(20) NOT NULL,
CONSTRAINT fk_movies_directors
FOREIGN KEY (`director_id`) REFERENCES `directors`(`id`),
`copyright_year` INT,
`length` DOUBLE,
CONSTRAINT fk_movies_genres
FOREIGN KEY (`genre_id`) REFERENCES `genres`(`id`),
CONSTRAINT fk_movies_categories
FOREIGN KEY (`category_id`) REFERENCES `categories`(`id`),
`rating` DOUBLE,
`notes` TEXT
);*/

CREATE TABLE `movies`.`movies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `director_id` INT NULL,
  `copyright_year` INT NULL,
  `length` DOUBLE NULL,
  `genre_id` INT NULL,
  `category_id` INT NULL,
  `rating` DOUBLE NULL,
  `notes` TEXT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `director_id`
    FOREIGN KEY (`director_id`)
    REFERENCES `movies`.`directors` (`id`),
  CONSTRAINT `genre_id`
    FOREIGN KEY (`genre_id`)
    REFERENCES `movies`.`genres` (`id`),
  CONSTRAINT `category_id`
    FOREIGN KEY (`category_id`)
    REFERENCES `movies`.`categories` (`id`)
    );


#Ex_12_Car Rental Database
CREATE DATABASE `car_rental`;
USE `car_rental`;

CREATE TABLE `categories` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`category` VARCHAR(20),
`daily_rate` DOUBLE,
`weekly_rate` DOUBLE,
`monthly_rate` DOUBLE,
`weekend_rate` DOUBLE
);

INSERT INTO `categories`(`category`)
VALUES
('category1'),
('category2'),
('category3');

CREATE TABLE `cars` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`plate_number` VARCHAR(10),
`make` DATE,
`model` VARCHAR(10),
`car_year` YEAR,
`category_id` INT,
`doors` INT,
`picture` BLOB,
`car_condition` TEXT,
`available` BOOLEAN,
CONSTRAINT fk_cars_categories
FOREIGN KEY (`category_id`) REFERENCES `categories`(`id`)
); 

INSERT INTO `cars`(`plate_number`, `model`, `category_id`)
VALUES
('CH4545', NULL, 2),
('CH4546', 'TESLA', 3),
('CH4547', 'TESLAA', 1);

CREATE TABLE `employees` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`first_name` VARCHAR(40),
`last_name` VARCHAR(40),
`title` VARCHAR(20),
`notes` TEXT
);

INSERT INTO `employees`(`first_name`, `last_name`, `title`)
VALUES
('name1', 'name1', 'title1'),
('name2', 'name2', 'title2'),
('name3', 'name3', 'title3');

CREATE TABLE `customers` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`driver_licence_number` INT,
`full_name` VARCHAR(50),
`address` TEXT,
`city` VARCHAR(20),
`zip_code` INT,
`notes` TEXT
);

INSERT INTO `customers`(`driver_licence_number`, `full_name`, `city`)
VALUES
(123456, 'name1', 'city1'),
(234567, 'name2', 'city2'),
(345678, 'name3', 'city3');

CREATE TABLE `rental_orders` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`employee_id` INT,
`customer_id` INT,
`car_id` INT,
`car_condition` TEXT,
`tank_level` INT,
`kilometrage_start` INT,
`kilometrage_end` INT,
`total_kilometrage` INT,
`start_date` DATETIME,
`end_date` DATETIME,
`total_days` INT,
`rate_applied` DOUBLE,
`tax_rate` DOUBLE,
`order_status` BOOLEAN,
`notes` TEXT,
CONSTRAINT fk_rental_orders_employees
FOREIGN KEY (`employee_id`) REFERENCES `employees`(`id`),
CONSTRAINT fk_rental_orders_customers
FOREIGN KEY (`customer_id`) REFERENCES `customers`(`id`),
CONSTRAINT fk_rental_orders_cars
FOREIGN KEY (`car_id`) REFERENCES `cars`(`id`)
);


#Ex_13_Basic insert
DROP DATABASE `soft_uni`;
CREATE DATABASE `soft_uni`;
USE `soft_uni`;

CREATE TABLE `towns` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(50)
);

CREATE TABLE `addresses` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`address_text` TEXT,
`town_id` INT,
CONSTRAINT fk_addresses_towns
FOREIGN KEY (`town_id`) REFERENCES `towns`(`id`)
);

CREATE TABLE `departments` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`name` VARCHAR(50)
);

CREATE TABLE `employees` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`first_name` VARCHAR(50),
`middle_name` VARCHAR(50),
`last_name` VARCHAR(50),
`job_title` VARCHAR(50),
`department_id` INT,
`hire_date` DATETIME,
`salary` DECIMAL,
`address_id` INT,
CONSTRAINT fk_employees_departments
FOREIGN KEY (`department_id`) REFERENCES `departments`(`id`),
CONSTRAINT fk_employees_addresses
FOREIGN KEY (`address_id`) REFERENCES `addresses`(`id`)
);

INSERT INTO `towns`(`id`, `name`)
VALUES
(1, 'Sofia'),
(2, 'Plovdiv'),
(3, 'Varna'),
(4, 'Burgas');

INSERT INTO `departments`(`id`, `name`)
VALUES
(1, 'Engineering'),
(2, 'Sales'),
(3, 'Marketing'),
(4, 'Software Development'),
(5, 'Quality Assurance');

INSERT INTO `employees` 
VALUES
(1, 'Ivan', 'Ivanov', 'Ivanov', '.NET Developer', 4, '2013-02-01', 3500.00, NULL),
(2, 'Petar', 'Petrov', 'Petrov', 'Senior Engineer', 1, '2004-03-02', 4000.00, NULL),
(3, 'Maria', 'Petrova', 'Ivanova', 'Intern', 5, '2016-08-28', 525.25, NULL),
(4, 'Georgi', 'Terziev', 'Ivanov', 'CEO', 2, '2007-12-09', 3000.00, NULL),
(5, 'Peter', 'Pan', 'Pan', 'Intern', 3, '2016-08-28', 599.88, NULL);


#Ex_14_Basic Select All Fields
SELECT * FROM `towns`;
SELECT * FROM `departments`;
SELECT * FROM `employees`;


#Ex_15_Basic Select All Fields and Order Them
SELECT * FROM `towns`
ORDER BY `name`;

SELECT * FROM `departments`
ORDER BY `name`;

SELECT * FROM `employees`
ORDER BY `salary` DESC;


#Ex_16_Basic Select Some Fields
SELECT `name` FROM `towns`
ORDER BY `name`;

SELECT `name` FROM `departments`
ORDER BY `name`;

SELECT `first_name`, `last_name`, `job_title`, `salary` FROM `employees`
ORDER BY `salary` DESC;


#Ex_17_Increase Employees Salary
UPDATE `employees`
SET `salary` = `salary` * 1.1;
SELECT `salary` FROM `employees`;

#Ex_18_Delete All Records
DROP DATABASE `soft_uni`;


