#Lab_01_Count Employees by Town
DELIMITER &&
CREATE FUNCTION ufn_count_employees_by_town(town_name VARCHAR(50))
RETURNS INT
DETERMINISTIC
BEGIN

	RETURN (SELECT COUNT(`employee_id`) AS `count`
	FROM `employees` AS e
	JOIN `addresses` AS a
	USING (`address_id`)
	JOIN `towns` AS t
	USING (`town_id`)
	WHERE t.`name` = town_name);

END
&&

SELECT ufn_count_employees_by_town('Sofia');


#Lab_02_Employees Promotion
DELIMITER &&&
CREATE PROCEDURE usp_raise_salaries(department_name VARCHAR(50))
BEGIN
	UPDATE `employees` AS e
	JOIN `departments` AS d
	USING (`department_id`)
	SET `salary` = `salary` * 1.05
	WHERE d.`name` = `department_name`;

END
&&&

CALL usp_raise_salaries('Sales');


#Lab_03_Employees Promotion by ID
DELIMITER&&
CREATE PROCEDURE usp_raise_salary_by_id(id INT) 
BEGIN
	IF( (SELECT COUNT(*) 
			FROM `employees` 
            WHERE `employee_id` = id) > 0)
	THEN
		UPDATE `employees` 
		SET `salary` = `salary` * 1.05
        WHERE `employee_id` = id
	END IF
END
&&


#Lab_04_Triggered
CREATE TABLE `deleted_employees` (
	`employee_id` INT PRIMARY KEY AUTO_INCREMENT,
	`first_name` VARCHAR(20),
	`last_name` VARCHAR(20),
	`middle_name` VARCHAR(20),
	`job_title` VARCHAR(50),
	`department_id` INT,
	`salary` DOUBLE
    );
    
    CREATE TRIGGER tr_deleted_employees 
    AFTER DELETE
	ON employees
	FOR EACH ROW
		BEGIN
		INSERT INTO deleted_employees (`first_name`, `last_name`, `middle_name`, `job_title`, `department_id`, `salary`)
		VALUES(OLD.first_name, OLD.last_name, OLD.middle_name, OLD.job_title, OLD.department_id, OLD.salary);
END;


	



