#Section 0: Database Overview
CREATE DATABASE `sgd`;
USE `sgd`;


#Section 1: Data Definition Language (DDL)
#1_Table Design
CREATE TABLE `addresses` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL
);

CREATE TABLE `offices` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `workspace_capacity` INT NOT NULL,
    `website` VARCHAR(50),
    `address_id` INT NOT NULL,
    CONSTRAINT fk_offices_addresses
    FOREIGN KEY (`address_id`) REFERENCES `addresses`(`id`)
);

CREATE TABLE `employees` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`first_name` VARCHAR(30) NOT NULL,
	`last_name` VARCHAR(30) NOT NULL,
    `age` INT NOT NULL,
    `salary` DECIMAL(10, 2) NOT NULL,
    `job_title` VARCHAR(20) NOT NULL,
    `happiness_level` CHAR(1) NOT NULL
);

CREATE TABLE `teams` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(40) NOT NULL,
    `office_id` INT NOT NULL,
    `leader_id` INT UNIQUE,
    CONSTRAINT fk_teams_offices
    FOREIGN KEY (`office_id`) REFERENCES `offices`(`id`),
    CONSTRAINT fk_teams_employees
    FOREIGN KEY (`leader_id`) REFERENCES `employees`(`id`)
); 

CREATE TABLE `games` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL UNIQUE,
    `description` TEXT,
    `rating` FLOAT NOT NULL DEFAULT 5.5,
    `budget` DECIMAL(10, 2) NOT NULL,
    `release_date` DATE,
    `team_id` INT NOT NULL,
    CONSTRAINT fk_games_teams
    FOREIGN KEY (`team_id`) REFERENCES `teams`(`id`)
);

CREATE TABLE `categories` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(10) NOT NULL
);

CREATE TABLE `games_categories` (
	`game_id` INT NOT NULL,
    `category_id` INT NOT NULL,
    CONSTRAINT pk_games_categories
		PRIMARY KEY (`game_id`, `category_id`),
    CONSTRAINT fk_games_categories_games
		FOREIGN KEY (`game_id`) REFERENCES `games`(`id`),
    CONSTRAINT fk_games_categories_categories
		FOREIGN KEY (`category_id`) REFERENCES `categories`(`id`)
);


#Section 2: Data Manipulation Language (DML) 
#2_Insert
INSERT INTO `games` (`name`, `rating`, `budget`, `team_id`)
SELECT 
	REVERSE(LOWER(SUBSTRING((`name`), 2))),
	`id`, 
    `leader_id` * 1000, 
    `id`
FROM `teams`
WHERE `id` BETWEEN 1 AND 9;

#3_Update
UPDATE `employees` AS e
SET e.`salary` = e.`salary` + 1000
WHERE e.`salary` < 5000
	AND e.`age` < 40
    AND e.`id` IN (SELECT t.`leader_id` FROM `teams` AS t);
    
#4_Delete
DELETE FROM `games`
WHERE `release_date` IS NULL 
	AND (SELECT COUNT(*) FROM games_categories WHERE game_id = id) = 0;
    

#Section 3: Querying
#5_Employees
SELECT `first_name`, `last_name`, `age`, `salary`, `happiness_level`
FROM `employees`
ORDER BY `salary`, `id`;

#6_Addresses of the teams
SELECT t.`name` AS `team_name`, a.`name` AS `address_name`, CHAR_LENGTH(a.`name`) AS `count_of_characters`
FROM `teams` AS t
JOIN `offices` AS o
ON t.`office_id` = o.`id`
JOIN `addresses` AS a
ON o.`address_id` = a.`id`
WHERE o.`website` IS NOT NULL
ORDER BY t.`name`, a.`name`;

#7_Categories Info
SELECT cat.`name`, COUNT(gc.`game_id`) AS `games_count`, ROUND(AVG(g.`budget`), 2) AS `avg_budget`, MAX(g.`rating`) AS `max_rating`
FROM `categories` AS cat
JOIN `games_categories` AS gc
ON cat.`id` = gc.`category_id`
JOIN `games` AS g
ON gc.`game_id` = g.`id`
GROUP BY cat.`name`
HAVING `max_rating` >= 9.5
ORDER BY `games_count` DESC, cat.`name`;

#8_Games of 2022
SELECT 
	g.`name`, 
    g.`release_date`,  
    CONCAT(LEFT(g.`description`, 10), '...') AS `summary`, 
	(CASE
		WHEN MONTH(g.`release_date`) IN	(01, 02, 03) THEN 'Q1'
		WHEN MONTH(g.`release_date`) IN	(04, 05, 06) THEN 'Q2'
		WHEN MONTH(g.`release_date`) IN	(07, 08, 09) THEN 'Q3'
		WHEN MONTH(g.`release_date`) IN	(10, 11, 12) THEN 'Q4'
    END) AS `Quarters`,
	t.`name` AS `team_name`
FROM `games` AS g
JOIN `teams` AS t
ON g.`team_id` = t.`id`
WHERE YEAR(g.`release_date`) = 2022
	AND MONTH(g.`release_date`) % 2 = 0
    AND g.`name` LIKE '%2'
GROUP BY g.`name`
ORDER BY `Quarters`;

#9_Full info for games
SELECT g.`name`, 
	(CASE
		WHEN g.`budget` < 50000 THEN 'Normal budget'
        ELSE 'Insufficient budget'
    END) AS `budget_level`,
    t.`name` AS `team_name`,
    a.`name` AS `address_name`
FROM `games` AS g
LEFT JOIN `teams` AS t
ON g.`team_id` = t.`id`
LEFT JOIN `offices` AS o
ON t.`office_id` = o.`id`
LEFT JOIN `addresses` AS a
ON o.`address_id` = a.`id`
WHERE g.`release_date` IS NULL
	AND g.`id` NOT IN (SELECT `game_id` FROM `games_categories`)
ORDER BY g.`name`;


#Section 4: Programmability
#10_Find all basic information for a game
DELIMITER &&
CREATE FUNCTION udf_game_info_by_name (game_name VARCHAR (20))
RETURNS VARCHAR(255)
DETERMINISTIC

SELECT udf_game_info_by_name('Bitwolf') AS info;

#11_Update budget of the games
DELIMITER &&
CREATE PROCEDURE udp_update_budget (min_game_rating FLOAT)
BEGIN
	UPDATE `games` AS g
    SET g.`budget` = g.`budget` + 100000,
		g.`release_date` = DATE_ADD(g.`release_date`, INTERVAL 1 YEAR)
	WHERE g.`id` NOT IN (SELECT `game_id` FROM `games_categories`)
		AND `rating` > min_game_rating
        AND `release_date` IS NOT NULL;
END &&
CALL udp_update_budget (8);

