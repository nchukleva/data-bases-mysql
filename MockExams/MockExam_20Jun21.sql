#Section 0: Database Overview
CREATE DATABASE `stc`;
USE `stc`;

#Section 1: Data Definition Language (DDL)
#1_Table Design
CREATE TABLE `categories` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(10) NOT NULL
);

CREATE TABLE `cars` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `make` VARCHAR(20) NOT NULL,
    `model` VARCHAR(20),
    `year` INT NOT NULL DEFAULT 0,
    `mileage` INT DEFAULT 0,
    `condition` CHAR(1) NOT NULL,
    `category_id` INT NOT NULL,
    CONSTRAINT fk_cars_categories
    FOREIGN KEY (`category_id`) REFERENCES `categories`(`id`)
); 

CREATE TABLE `drivers` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `first_name` VARCHAR(30) NOT NULL,
    `last_name` VARCHAR(30) NOT NULL,
    `age` INT NOT NULL,
    `rating` FLOAT DEFAULT 5.5
);

CREATE TABLE `cars_drivers` (
	`car_id` INT NOT NULL,
    `driver_id` INT NOT NULL,
    CONSTRAINT pk_cars_drivers 
    PRIMARY KEY (`car_id`, `driver_id`),
    CONSTRAINT fk_cars_drivers_cars 
    FOREIGN KEY (`car_id`) REFERENCES `cars`(`id`),
    CONSTRAINT fk_cars_drivers_drivers 
    FOREIGN KEY (`driver_id`) REFERENCES `drivers`(`id`)
);

CREATE TABLE `addresses` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL
);

CREATE TABLE `clients` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `full_name` VARCHAR(50) NOT NULL,
    `phone_number` VARCHAR(20) NOT NULL
);

CREATE TABLE `courses` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`from_address_id` INT NOT NULL,
    `start` DATETIME NOT NULL,
    `bill` DECIMAL(10, 2) DEFAULT 10,
    `car_id` INT NOT NULL,
    `client_id` INT NOT NULL,
    CONSTRAINT fk_courses_addresses FOREIGN KEY (`from_address_id`) REFERENCES `addresses`(`id`),
    CONSTRAINT fk_courses_cars FOREIGN KEY (`car_id`) REFERENCES `cars`(`id`),
    CONSTRAINT fk_courses_clients FOREIGN KEY (`client_id`) REFERENCES `clients`(`id`)
); 

#Section 2: Data Manipulation Language (DML)
#2_Insert
INSERT INTO `clients` (`full_name`, `phone_number`)
SELECT CONCAT(`first_name`, ' ', `last_name`) AS `full_name`, CONCAT('(088) 9999', `id` * 2) AS `phone_number`
FROM `drivers`
WHERE `id` BETWEEN 10 AND 20;

#3_Update
UPDATE `cars` AS c
SET c.`condition` = 'C'
WHERE (c.`mileage` >= 800000 OR c.`mileage` IS NULL)
    AND c.`year` <= 2010
    AND c.`model` != 'Mercedes-Benz';

#4_Delete
DELETE FROM `clients` 
WHERE CHAR_LENGTH(`full_name`) > 3
	AND `id` NOT IN (SELECT `client_id` FROM `courses`);


#Section 3: Querying
#5_Cars
SELECT `make`, `model`, `condition` 
FROM `cars`
ORDER BY `id`;

#6_Drivers and Cars
SELECT d.`first_name`, d.`last_name`, c.`make`, c.`model`, c.`mileage`
FROM `drivers` AS d
JOIN `cars_drivers` AS cd
ON d.`id` = cd.`driver_id`
JOIN `cars` AS c
ON cd.`car_id` = c.`id`
WHERE c.`mileage` IS NOT NULL
ORDER BY c.`mileage` DESC, d.`first_name`;

#7_Number of courses for each car
SELECT c.`id` AS `car_id`, c.`make`, c.`mileage`, COUNT(co.`id`) AS `count_of_courses`, ROUND(AVG(co.`bill`), 2) AS `avg_bill`
FROM `cars` AS c
LEFT JOIN `courses` AS co
ON c.`id` = co.`car_id`
GROUP BY c.`id`
HAVING count_of_courses != 2
ORDER BY `count_of_courses` DESC, c.`id`;

#8_Regular clients
SELECT c.`full_name`, COUNT(co.`client_id`) AS `count_of_cars`, SUM(co.`bill`) AS `total_sum`
FROM `clients` AS c
JOIN `courses` AS co
ON c.`id` = co.`client_id`
WHERE c.`full_name` LIKE '_a%' 
GROUP BY co.`client_id`
HAVING `count_of_cars` > 1
ORDER BY c.`full_name`;

#9_Full information of courses
SELECT addr.`name`, 
	(CASE
		WHEN HOUR(courses.`start`) BETWEEN 6 AND 20 THEN 'Day' 
        ELSE 'Night' 
	END) AS `day_time`,
	courses.`bill`, clients.`full_name`, cars.`make`, cars.`model`, cat.`name` AS `category_name`
FROM `addresses` AS addr
JOIN `courses` AS courses
ON addr.`id` = courses.`from_address_id`
JOIN `clients` AS clients
ON courses.`client_id` = clients.`id`
JOIN `cars` AS cars
ON courses.`car_id` = cars.`id`
JOIN `categories` AS cat
ON cars.`category_id` = cat.`id`
ORDER BY courses.`id`;


#Section 4: Programmability
#10_Find all courses by client’s phone number
DELIMITER &&
CREATE FUNCTION udf_courses_by_client (phone_num VARCHAR (20))
RETURNS INT
DETERMINISTIC
BEGIN
	RETURN
		(SELECT COUNT(c.`client_id`)
		FROM `courses` AS c
		JOIN `clients` AS cl
		ON c.`client_id` = cl.`id`
		WHERE cl.`phone_number` = phone_num);
END &&
SELECT udf_courses_by_client ('(803) 6386812') as `count`; 

#11_Full info for address
DELIMITER &&
CREATE PROCEDURE udp_courses_by_address(address_name VARCHAR(100))
BEGIN
	SELECT a.`name`, c.`full_name` AS `full_names`,
		(CASE 
			WHEN crs.`bill` <= 20 THEN 'Low'
			WHEN crs.`bill` > 20 AND crs.`bill`	<= 30 THEN 'Medium'
			ELSE 'High'
		END) AS `level_of_bill`,
    car.`make`,
    car.`condition`,
    cat.`name` AS `cat_name`
FROM `addresses` AS a
JOIN `courses` AS crs ON a.`id` = crs.`from_address_id`
JOIN `clients` AS c ON crs.`client_id` = c.`id`
JOIN `cars` AS car ON car.`id` = crs.`car_id`
JOIN `categories` AS cat ON cat.`id` = car.`category_id`
WHERE a.`name` = address_name
ORDER BY car.`make`, c.`full_names`;
END &&
CALL udp_courses_by_address('66 Thompson Drive');
