#Section 0: Database Overview
CREATE DATABASE `fsd`;
USE `fsd`;
#SELECT @@sql_mode;
#SET sql_mode = ' ';


#Section 1: Data Definition Language (DDL)
CREATE TABLE `countries` (
	`id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL
    );
    
CREATE TABLE `towns` (
	`id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `country_id` INT NOT NULL,
    CONSTRAINT fk_towns_countries
    FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`)
    );
    
CREATE TABLE `stadiums` (
	`id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `capacity` INT NOT NULL,
    `town_id` INT NOT NULL,
    CONSTRAINT fk_stadiums_towns
    FOREIGN KEY (`town_id`) REFERENCES `towns`(`id`)
);

CREATE TABLE `teams` (
	`id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `established` DATE NOT NULL,
    `fan_base` BIGINT NOT NULL DEFAULT 0,
    `stadium_id` INT NOT NULL,
    CONSTRAINT fk_teams_stadiums
    FOREIGN KEY (`stadium_id`) REFERENCES `stadiums`(`id`)
);

CREATE TABLE `skills_data` (
	`id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `dribbling` INT DEFAULT 0,
    `pace` INT DEFAULT 0,
    `passing` INT DEFAULT 0,
    `shooting` INT DEFAULT 0,
    `speed` INT DEFAULT 0,
    `strength` INT DEFAULT 0
);

CREATE TABLE `players` (
	`id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `first_name` VARCHAR(10) NOT NULL,
    `last_name` VARCHAR(20) NOT NULL,
    `age` INT NOT NULL DEFAULT 0,
    `position` CHAR(1) NOT NULL,
    `salary` DECIMAL(8, 2) NOT NULL DEFAULT 0,
    `hire_date` DATETIME,
    `skills_data_id` INT NOT NULL,
    `team_id` INT,
    CONSTRAINT fk_players_skills_data
    FOREIGN KEY (`skills_data_id`) REFERENCES `skills_data`(`id`),
    CONSTRAINT fk_players_teams
    FOREIGN KEY (`team_id`) REFERENCES `teams`(`id`)
);
    
CREATE TABLE `coaches` (
	`id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `first_name` VARCHAR(10) NOT NULL,
    `last_name` VARCHAR(20) NOT NULL,
    `salary` DECIMAL(8, 2) NOT NULL DEFAULT 0,
    `coach_level` INT NOT NULL DEFAULT 0
);

CREATE TABLE `players_coaches` (
	`player_id` INT(11),
	`coach_id` INT(11),
    CONSTRAINT pk_players_coaches
    PRIMARY KEY (`player_id`, `coach_id`),
    CONSTRAINT fk_players_coaches_players
    FOREIGN KEY (`player_id`) REFERENCES `players`(`id`),
    CONSTRAINT fk_players_coaches_coaches
    FOREIGN KEY (`coach_id`) REFERENCES `coaches`(`id`)
);


#Section 2: Data Manipulation Language (DML)
#2_Insert
INSERT INTO `coaches` (`first_name`, `last_name`, `salary`, `coach_level`)
SELECT `first_name`, `last_name`, `salary` * 2, CHAR_LENGTH(`first_name`) AS `coach_level` FROM `players` WHERE `age` >= 45;


#3_Update
UPDATE `coaches`
SET `coach_level` = `coach_level` + 1
WHERE `first_name` LIKE 'A%' AND (
	SELECT COUNT(*) FROM `players_coaches` WHERE `coach_id` = `id`) > 0;

UPDATE `coaches` AS c
JOIN `players_coaches` AS pc
ON pc.`coach_id` = c.`id`
JOIN `players` AS p
ON p.`id` = pc.`player_id`
SET `coach_level` = `coach_level` + 1
WHERE c.`first_name` LIKE 'A%';

#4_Delete
DELETE FROM `players`
WHERE `age` >= 45;


#Section 3: Querying
#5_Players
SELECT p.`first_name`, p.`age`, p.`salary`
FROM `players` AS p
ORDER BY p.`salary` DESC;

#6_Young offense players without contract
SELECT p.`id`, CONCAT_WS(' ', p.`first_name`, p.`last_name`) AS `full_name`, p.`age`, p.`position`, p.`hire_date`
FROM `players` AS p
JOIN `skills_data` AS sd
ON p.`skills_data_id` = sd.`id`
WHERE p.`age` < 23 
	AND p.`position` = 'A' 
    AND p.`hire_date` IS NULL 
    AND sd.`strength` > 50
ORDER BY p.`salary`, p.`age`;


#7_Detail info for all teams
SELECT t.`name` AS `team_name`, t.`established`, t.`fan_base`, COUNT(p.`id`) AS `players_count`
FROM `teams` AS t
LEFT JOIN `players` AS p
ON t.`id` = p.`team_id`
GROUP BY t.`name`
ORDER BY `players_count` DESC, t.`fan_base` DESC;

/*SELECT t.`name` AS `team_name`, t.`established`, t.`fan_base`, 
	(SELECT COUNT(*) 
	FROM `players` 
    WHERE `team.id = t.`id`) AS `players_count`
FROM `teams` AS t
ORDER BY `players_count` DESC, t.`fan_base` DESC;*/

#8_The fastest player by towns
SELECT MAX(sd.`speed`) AS `max_speed`, t.`name` AS `town_name`
FROM `skills_data` AS sd
RIGHT JOIN `players` AS p
ON sd.`id` = p.`skills_data_id`
RIGHT JOIN `teams` AS tm
ON tm.`id` = p.`team_id`
RIGHT JOIN `stadiums` AS s
ON tm.`stadium_id` = s.`id`
RIGHT JOIN `towns` AS t
ON s.`town_id` = t.`id`
WHERE tm.`name` != 'Devify'
GROUP BY t.`id`
ORDER BY `max_speed` DESC, `town_name`;

#9.	Total salaries and players by country
SELECT c.`name`, COUNT(p.`id`) AS `total_count_of_players`, SUM(p.`salary`) AS `total_sum_of_salaries`
FROM `players` AS p
RIGHT JOIN `teams` AS tm
ON p.`team_id` = tm.`id` 
RIGHT JOIN `stadiums` AS s
ON tm.`stadium_id` = s.`id`
RIGHT JOIN `towns` AS t
ON s.`town_id` = t.`id`
RIGHT JOIN `countries` AS c
ON t.`country_id` = c.`id`
GROUP BY c.`name`
ORDER BY `total_count_of_players` DESC, c.`name`;

#Section 4: Programmability
#10_Find all players that play on stadium
DELIMITER &&
CREATE FUNCTION udf_stadium_players_count (stadium_name VARCHAR(30))
RETURNS INT
DETERMINISTIC
BEGIN
	RETURN
		(SELECT COUNT(*) AS `count`
		FROM `players` AS p
		JOIN `teams` AS t
		ON p.`team_id` = t.`id`
		JOIN `stadiums` AS s
		ON t.`stadium_id` = s.`id`
		WHERE s.`name` = stadium_name);
END &&
SELECT udf_stadium_players_count ('Linklinks');

#11_Find good playmaker by teams
DELIMITER &&
CREATE PROCEDURE udp_find_playmaker (min_dribble_points INT, team_name VARCHAR(45))
BEGIN
	SELECT CONCAT(p.`first_name`, ' ', p.`last_name`) AS `full_name`, p.`age`, p.`salary`, sd.`dribbling`, sd.`speed`, t.`name` AS `team_name`
    FROM `players` AS p
    JOIN `skills_data` AS sd
    ON p.`skills_data_id` = sd.`id`
    JOIN `teams` AS t
    ON p.`team_id` = t.`id`
    WHERE sd.`dribbling` > min_dribble_points 
		AND t.`name` = team_name 
        AND `speed` > (SELECT AVG(`speed`) FROM `skills_data`)
    ORDER BY sd.`speed` DESC
    LIMIT 1;
    
END &&


