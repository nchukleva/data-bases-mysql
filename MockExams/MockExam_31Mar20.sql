#Section 0: Database Overview
CREATE DATABASE `instd`;
USE `instd`;

#Section 1: Data Definition Language (DDL)
#01_Table Design
CREATE TABLE `users` (
	`id` INT PRIMARY KEY,
    `username` VARCHAR(30) NOT NULL UNIQUE,
    `password` VARCHAR(30) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `gender` CHAR(1) NOT NULL,
    `age` INT NOT NULL,
    `job_title` VARCHAR(40) NOT NULL,
    `ip` VARCHAR(30) NOT NULL
);

CREATE TABLE `addresses` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `address` VARCHAR(30) NOT NULL,
    `town` VARCHAR(30) NOT NULL,
    `country` VARCHAR(30) NOT NULL,
    `user_id` INT NOT NULL,
    CONSTRAINT fk_addresses_users
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
);

CREATE TABLE `photos` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `description` TEXT NOT NULL,
    `date` DATETIME NOT NULL,
    `views` INT NOT NULL DEFAULT 0
);

CREATE TABLE `users_photos` (
	`user_id` INT NOT NULL,
    `photo_id` INT NOT NULL,
	CONSTRAINT pk_users_photos
    PRIMARY KEY (`user_id`, `photo_id`),
    CONSTRAINT fk_users_photos_users
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`),
    CONSTRAINT fk_users_photos_photos
    FOREIGN KEY (`photo_id`) REFERENCES `photos`(`id`)
);

CREATE TABLE `likes` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`user_id` INT,
    `photo_id` INT,
    CONSTRAINT fk_likes_users
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`),
    CONSTRAINT fk_likes_photos
    FOREIGN KEY (`photo_id`) REFERENCES `photos`(`id`)
);

CREATE TABLE `comments` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`comment` VARCHAR(255) NOT NULL,
	`date` DATETIME NOT NULL,
    `photo_id` INT NOT NULL,
    CONSTRAINT fk_comments_photos
    FOREIGN KEY (`photo_id`) REFERENCES `photos`(`id`)
);


#Section 2: Data Manipulation Language (DML)
#02_Insert
INSERT INTO `addresses` (`address`, `town`, `country`, `user_id`)
SELECT u.`username`, u.`password`, u.`ip`, u.`age` 
FROM `users` AS u
WHERE u.`gender` = 'M';

#03_Update
SELECT * FROM `addresses`;
UPDATE `addresses` AS a
SET `country` = (
	CASE
		WHEN `country` LIKE 'B%' THEN 'Blocked'
		WHEN `country` LIKE 'T%' THEN 'Test'
		WHEN `country` LIKE 'P%' THEN 'In Progress'
        ELSE `country`
	END);
SELECT * FROM `addresses`;	

#04_Delete
DELETE 
FROM `addresses`
WHERE `id` % 3 = 0;


#Section 3: Querying
#05_Users
SELECT `username`, `gender`, `age` 
FROM `users`
ORDER BY `age` DESC, `username`;

#06_Extract 5 Most Commented Photos
SELECT p.`id`, p.`date` AS `date_and_time`, p.`description`, COUNT(c.`id`) AS `commentsCount`
FROM `photos` AS p
JOIN `comments` AS c
ON p.`id` = c.`photo_id`
GROUP BY `id`
ORDER BY `commentsCount` DESC, `id`
LIMIT 5;

#07_Lucky Users
SELECT CONCAT_WS(' ', u.`id`, u.`username`) AS `id_username`, u.`email`
FROM `users` AS u
JOIN `users_photos` AS up
#ON u.`id` = up.`user_id`
ON u.`id` = up.`user_id` AND u.`id` = up.`photo_id`;
#WHERE up.`user_id` = up.`photo_id`;

#08_Count Likes and Comments
SELECT p.`id`, COUNT(DISTINCT l.`id`) AS 'likes_count', COUNT(DISTINCT c.`id`) AS 'comments_count'
FROM photos AS p
LEFT JOIN `likes` AS l 
ON p.`id` = l.`photo_id`
LEFT JOIN `comments` AS c 
ON p.`id` = c.`photo_id`
GROUP BY p.`id`
ORDER BY `likes_count` DESC , `comments_count` DESC , p.`id` ASC;

#09_The Photo on the Tenth Day of the Month
SELECT CONCAT(LEFT(p.`description`, 30), '...') AS `summary`, p.`date`
FROM `photos` AS p
WHERE DAY(p.`date`) = 10
GROUP BY p.`id`, p.`date`
ORDER BY p.`date` DESC;


#Section 4: Programmability
#10_Get User’s Photos Count
DELIMITER &&
CREATE FUNCTION udf_users_photos_count (username VARCHAR(30))
RETURNS INT
DETERMINISTIC
BEGIN
	RETURN 
		(SELECT COUNT(*) 
        FROM `users` AS u
        JOIN `users_photos` AS up
        ON u.`id` = up.`user_id`
        WHERE u.`username` = username);		
END &&
SELECT udf_users_photos_count('ssantryd');

#11_Increase User Age
DELIMITER &&
CREATE PROCEDURE udp_modify_user (address VARCHAR(30), town VARCHAR(30))
BEGIN
	IF((SELECT a.`addresses` 
		FROM `addresses` AS a
		WHERE a.`address` = address) IS NOT NULL)
    THEN 
		UPDATE `uesers` AS u
		JOIN `addresses` AS a
		ON u.`id` = a.`user_id`
		SET u.`age` = u.`age` + 10
		WHERE a.`address` = address AND a.`town` = town;
	END IF;
END &&
CALL udp_modify_user ('97 Valley Edge Parkway', 'Divinópolis');


















