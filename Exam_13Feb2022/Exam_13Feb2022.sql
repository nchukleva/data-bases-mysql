#Section 0: Database Overview
CREATE DATABASE `online_store`;
USE `online_store`;


#Section 1: Data Definition Language (DDL)
#01_Table Design
CREATE TABLE `brands` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(40) NOT NULL UNIQUE
);

CREATE TABLE `categories` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(40) NOT NULL UNIQUE
);

CREATE TABLE `reviews` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `content` TEXT,
    `rating` DECIMAL(10, 2) NOT NULL,
    `picture_url` VARCHAR(80) NOT NULL,
    `published_at` DATETIME NOT NULL
);

CREATE TABLE `products` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(40) NOT NULL,
    `price` DECIMAL(19, 2) NOT NULL,
    `quantity_in_stock` INT,
    `description` TEXT,
    `brand_id` INT NOT NULL,
    `category_id` INT NOT NULL,
    `review_id` INT,
    CONSTRAINT fk_products_brands
    FOREIGN KEY (`brand_id`) REFERENCES `brands`(`id`),
    CONSTRAINT fk_products_categories
    FOREIGN KEY (`category_id`) REFERENCES `categories`(`id`),
    CONSTRAINT fk_products_reviews
    FOREIGN KEY (`review_id`) REFERENCES `reviews`(`id`)
);

CREATE TABLE `customers` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `first_name` VARCHAR(20) NOT NULL,
    `last_name` VARCHAR(20) NOT NULL,
    `phone` VARCHAR(30) NOT NULL UNIQUE,
    `address` VARCHAR(60) NOT NULL,
    `discount_card` BIT NOT NULL DEFAULT 0
);

CREATE TABLE `orders` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
    `order_datetime` DATETIME NOT NULL,
    `customer_id` INT NOT NULL,
    CONSTRAINT fk_orders_customers
    FOREIGN KEY (`customer_id`) REFERENCES `customers`(`id`)
);

CREATE TABLE `orders_products` (
	`order_id` INT,
    `product_id` INT,
    CONSTRAINT fk_orders_products_orders
    FOREIGN KEY (`order_id`) REFERENCES `orders`(`id`),
    CONSTRAINT fk_orders_products_products
    FOREIGN KEY (`product_id`) REFERENCES `products`(`id`)
);


#Section 2: Data Manipulation Language (DML)
#02_Insert
INSERT INTO `reviews` (`content`, `picture_url`, `published_at`, `rating`)
SELECT LEFT(`description`, 15), REVERSE(`name`), '2010-10-10', ROUND(`price` / 8, 2)
FROM `products`
WHERE `id` >= 5;

#03_Update
UPDATE `products`
SET `quantity_in_stock` = `quantity_in_stock` - 5
WHERE `quantity_in_stock` >= 60 AND `quantity_in_stock` <= 70;

#04_Delete
DELETE FROM `customers`
WHERE `id` NOT IN (SELECT `customer_id` FROM `orders`);


#Section 3: Querying
#05_Categories
SELECT * FROM `categories`
ORDER BY `name` DESC;

#06_Quantity
SELECT `id`, `brand_id`, `name`, `quantity_in_stock`
FROM `products`
WHERE `price` > 1000 AND `quantity_in_stock` < 30
ORDER BY `quantity_in_stock`, `id`;

#07_Review
SELECT * FROM `reviews`
WHERE `content` LIKE 'My%' AND CHAR_LENGTH(`content`) > 61
ORDER BY `rating` DESC;

#08_First customers
SELECT CONCAT(c.`first_name`, ' ', `last_name`) AS `full_name`, c.`address`, o.`order_datetime`
FROM `customers` AS c
JOIN `orders` AS o
ON c.`id` = o.`customer_id`
WHERE YEAR(o.`order_datetime`) <= 2018
ORDER BY `full_name` DESC;

#09_Best categories
SELECT COUNT(p.`category_id`) AS `items_count`, c.`name`, SUM(p.`quantity_in_stock`) AS `total_quantity`
FROM `categories` AS c
JOIN `products` AS p
ON c.`id` = p.`category_id`
GROUP BY p.`category_id`
ORDER BY `items_count` DESC, `total_quantity`
LIMIT 5;


#Section 4: Programmability
#10_Extract client cards count
DELIMITER &&
CREATE FUNCTION udf_customer_products_count(client_name VARCHAR(30))
RETURNS INT
DETERMINISTIC
BEGIN
	RETURN
		(SELECT COUNT(op.`product_id`) AS `total_products`
		FROM `orders` AS o
		JOIN `customers` AS c
		ON o.`customer_id` = c.`id`
		JOIN `orders_products` AS op
		ON o.`id` = op.`order_id`
		WHERE `first_name` = client_name);
END &&
SELECT c.first_name,c.last_name, udf_customer_products_count('Shirley') as `total_products` FROM customers c
WHERE c.first_name = 'Shirley';

#11_Reduce price
DELIMITER &&
CREATE PROCEDURE udp_reduce_price(category_name VARCHAR(50))
BEGIN
	UPDATE `products` AS p
    JOIN `reviews` AS r
	ON p.`review_id` = r.`id`
    JOIN `categories` AS c
    ON c.`id` = p.`category_id`
    SET p.`price` = p.`price` - (p.`price` * 30/100)
	WHERE r.`rating` < 4 AND c.`name` = category_name;
END &&
