#Ex_01_Find All Information About Departments
SELECT * FROM `departments`
ORDER BY `department_id`;

#Ex_02_Find all Department Names
SELECT `name` FROM `departments`
ORDER BY `department_id`;

#Ex_03_Find salary of Each Employee
SELECT * FROM `employees`;
SELECT `first_name`, `last_name`, `salary` FROM `employees`
ORDER BY `employee_id`;

#Ex_04_Find Full Name of Each Employee
SELECT * FROM `employees`;
SELECT `first_name`, `middle_name`, `last_name` FROM `employees`
ORDER BY `employee_id`;

#SELECT * FROM `employees`
# WHERE `middle_name` IS NULL;
#WHERE `middle_name` IS NOT NULL;

#Ex_05_Find Email Address of Each Employee
SELECT CONCAT(`first_name`, '.', `last_name`, '@softuni.bg') AS `full_email_address` FROM `employees`; 

#Ex_06_Find All Different Employee's Salaries
SELECT DISTINCT `salary` FROM `employees`;

#Ex_07_Find all Information About Employees
SELECT * FROM `employees`
WHERE `job_title` = 'Sales Representative';

#Ex_08_Find Names of All Employees by salary in Range
SELECT `first_name`, `last_name`, `job_title` FROM `employees`
WHERE `salary` BETWEEN 20000 AND 30000
ORDER BY `employee_id`;

#Ex_09_Find Names of All Employees
SELECT CONCAT_WS(' ', `first_name`, `middle_name`, `last_name`) AS `Full Name` FROM `employees`
WHERE `salary` IN (25000, 14000, 12500, 23600);

#Ex_10_Find All Employees Without Manager
SELECT `first_name`, `last_name` FROM `employees`
WHERE `manager_id` IS NULL;

#Ex_11_Find All Employees with salary More Than 50000
SELECT `first_name`, `last_name`, `salary` FROM `employees`
WHERE `salary` > 50000
ORDER BY `salary` DESC;

#Ex_12_Find 5 Best Paid Employees
SELECT `first_name`, `last_name` FROM `employees`
ORDER BY `salary` DESC
LIMIT 5;

#Ex_13_Find All Employees Except Marketing
SELECT `first_name`, `last_name` FROM `employees`
WHERE `department_id` != 4;

#Ex_14_Sort Employees Table
SELECT * FROM `employees`
ORDER BY `salary` DESC, `first_name`, `last_name` DESC, `middle_name`;

#Ex_15_Create View Employees with Salaries
CREATE VIEW `v_employees_salaries` AS SELECT `first_name`, `last_name`, `salary` FROM `employees`;
SELECT * FROM `v_employees_salaries`;

#Ex_16_Create View Employees with Job Titles
CREATE VIEW `v_employees_job_titles` AS 
SELECT CONCAT_WS(' ', `first_name`, `middle_name`, `last_name`) AS `full_name`, `job_title` FROM `employees`;
SELECT * FROM `v_employees_job_titles`;

#eX_17_Distinct Job Titles
SELECT DISTINCT `job_title` FROM `employees`
ORDER BY `job_title`;

#Ex_18_Find First 10 Started Projects
SELECT * FROM `projects`
ORDER BY `start_date`, `name`
LIMIT 10;

#Ex_19_Last 7 Hired Employees
SELECT `first_name`, `last_name`, `hire_date` FROM `employees`
ORDER BY `hire_date` DESC
LIMIT 7;

#Ex_20_Increase Salaries
UPDATE `employees` 
SET `salary` = `salary` * 1.12
WHERE `department_id` IN (1, 2, 4, 11);
SELECT `salary` FROM `employees`;






