#Ex_12_Employees Minimum Salaries
SELECT `department_id`, MIN(`salary`) AS `minimum_salary`
FROM `employees`
WHERE `department_id` IN(2, 5, 7) AND YEAR(`hire_date`) >= 2000
GROUP BY `department_id`
ORDER BY `department_id`;


#Ex_13_Employees Average Salaries
CREATE TABLE `high_paid_emoployee` AS
SELECT * FROM `employees`
WHERE `salary` > 30000 AND `manager_id` != 42;

UPDATE `high_paid_emoployee`
SET `salary` = `salary` + 5000
WHERE `department_id` = 1;

SELECT `department_id`, AVG(`salaray`) AS `avg_salary`
FROM `high_paid_emoployee`
GROUP BY `department_id`;


#Ex_14_Employees Maximum Salaries
SELECT `department_id`, MAX(`salary`) AS `max_salary`
FROM `employees`
GROUP BY `department_id`
HAVING `max_salary` NOT BETWEEN 30000 AND 70000
ORDER BY `department_id`;


#Ex_15_Employees Count Salaries
SELECT COUNT(`employee_id`) AS ' '
FROM `employees`
WHERE `manager_id` IS NULL;


#Ex_16_3rd Highest Salary*
SELECT e.`department_id`, 
(
	SELECT DISTINCT e2.`salary` FROM `employees` AS e2
    WHERE e2.`department_id` = e.`department_id`
    ORDER BY e2.`salary` DESC
    LIMIT 1 OFFSET 2
) AS `third_highest_salary`
FROM `employees` AS e
GROUP BY e.`department_id`
HAVING `third_highest_salary` IS NOT NULL
ORDER BY e.`department_id`;


#Ex_17_Salary Challenge**
SELECT e.`first_name`, e.`last_name`, e.`department_id`
FROM `employees` AS e
WHERE e.`salary` > 
(
	SELECT AVG(e2.`salary`) 
	FROM `employees` AS e2
	WHERE e2.`department_id` = e.`department_id`
)
ORDER BY `department_id`, `employee_id`
LIMIT 10;


#Ex_18_Departments Total Salaries
SELECT `department_id`, SUM(`salary`) AS `total_salary`
FROM `employees`
GROUP BY `department_id`
ORDER BY `department_id`;



