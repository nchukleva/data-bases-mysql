#Ex_01_Records' Count
SELECT COUNT(`id`) AS `count`
#SELECT COUNT(*) AS `count`
FROM `wizzard_deposits`;


#Ex_02_Longest Magic Wand
SELECT MAX(`magic_wand_size`) AS `longest_magic_wand`
FROM `wizzard_deposits`;


#Ex_03_Longest Magic Wand Per Deposit Groups
SELECT * FROM `wizzard_deposits`;

SELECT `deposit_group`, MAX(`magic_wand_size`) AS `longest_magic_wand`
FROM `wizzard_deposits`
GROUP BY `deposit_group`
ORDER BY `longest_magic_wand`, `deposit_group`;


#Ex_04_Smallest Deposit Group Per Magic Wand Size
SELECT `deposit_group`
FROM `wizzard_deposits`
GROUP BY `deposit_group`
HAVING AVG(`magic_wand_size`)
#ORDER BY AVG(`magic_wand_size`)
LIMIT 1;


#Ex_05_Deposits Sum
SELECT `deposit_group`, SUM(`deposit_amount`) AS `total_sum`
FROM `wizzard_deposits`
GROUP BY `deposit_group`
ORDER BY `total_sum`;


#Ex_06_Deposits Sum for Ollivander Family
SELECT `deposit_group`, SUM(`deposit_amount`) AS `total_sum`
FROM `wizzard_deposits`
WHERE `magic_wand_creator` = 'Ollivander family'
GROUP BY `deposit_group`
ORDER BY `deposit_group`;


#Ex_07_Deposits Filter
SELECT `deposit_group`, SUM(`deposit_amount`) AS `total_sum`
FROM `wizzard_deposits`
WHERE `magic_wand_creator` = 'Ollivander family'
GROUP BY `deposit_group`
HAVING `total_sum` < 150000
ORDER BY `total_sum` DESC;


#Ex_08_Deposit Charge
SELECT `deposit_group`, `magic_wand_creator`, MIN(`deposit_charge`) AS `min_deposit_charge`
FROM `wizzard_deposits`
GROUP BY `deposit_group`, `magic_wand_creator`
ORDER BY `magic_wand_creator`, `deposit_group`;


#Ex_09_Age Groups
SELECT (
	CASE
		WHEN `age` <= 10 THEN '[0-10]'
		WHEN `age` <= 20 THEN '[11-20]'
		WHEN `age` <= 30 THEN '[21-30]'
		WHEN `age` <= 40 THEN '[31-40]'
		WHEN `age` <= 50 THEN '[41-50]'
		WHEN `age` <= 60 THEN '[51-60]'
		ELSE '[61+]'
    END 
    ) AS `age_group`, COUNT(*) AS `wizard_count`
FROM `wizzard_deposits`
GROUP BY `age_group`
ORDER BY `age_group`;


#Ex_10_First Letter
SELECT LEFT(`first_name`, 1) AS `first_letter`
FROM `wizzard_deposits`
WHERE `deposit_group` = 'Troll Chest'
GROUP BY `first_letter`
ORDER BY `first_letter`;


#Ex_11_Average Interest
SELECT `deposit_group`, `is_deposit_expired`, AVG(`deposit_interest`) AS `average_interest`
FROM `wizzard_deposits`
WHERE `deposit_start_date` > '1985-01-01'
GROUP BY `deposit_group`, `is_deposit_expired`
ORDER BY `deposit_group` DESC, `is_deposit_expired`;



