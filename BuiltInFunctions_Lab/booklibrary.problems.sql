#Lab_01_Find Book Titles
SELECT `title` FROM `books`
WHERE SUBSTRING(`title`, 1, 3) = 'The'
ORDER BY `id`;


#Lab_2_Replace Titles
SELECT REPLACE(`title`, 'The', '***') AS `new title`, `title` FROM `books`
WHERE SUBSTRING(`title`, 1, 3) = 'The';

SELECT REPLACE(`title`, 'The', '***') FROM `books`
WHERE SUBSTRING(`title`, 1, 3) = 'The';

SELECT LENGTH(`title`) AS `title_length`, `title`
FROM `books`;

SELECT INSERT(`title`, LOCATE('Mystery', `title`), 0, ' INSERTED ') AS title 
FROM `books`; 


#Lab_3_Sum Cost of All Books
SELECT SUM(ROUND(`cost`, 2)) FROM `books`;
SELECT SUM(`cost`) FROM `books`;


#Lab_4_Days Lived
SELECT * FROM `authors`;

#Calculates days lived between `born` and `died`
SELECT CONCAT_WS(' ', `first_name`, `last_name`) AS `Full name`,
TIMESTAMPDIFF(DAY, `born`, `died`) AS `Days Lived`
FROM `authors`;

#Calculates days lived between `born` and `died`. If `died` is null calculates untill todays date
SELECT CONCAT_WS(' ', `first_name`, `last_name`) AS `Full name`,
TIMESTAMPDIFF(DAY, `born`, IF(`died` IS NULL, NOW(), `died`)) AS `Days Lived`
FROM `authors`;


#Lab_5_Harry Potter Books
SELECT `title` FROM `books`
WHERE `title` LIKE '%Harry%'
ORDER BY `id`;



