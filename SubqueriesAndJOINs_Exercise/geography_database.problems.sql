#Ex_12_Highest Peaks in Bulgaria
SELECT `country_code`, m.`mountain_range`, p.`peak_name`, p.`elevation`
FROM `countries` AS c
JOIN `mountains_countries` AS mc
USING(`country_code`)
JOIN `mountains` AS m
ON mc.`mountain_id` = m.`id`
JOIN `peaks` AS p
ON m.`id` = p.`mountain_id`
WHERE c.`country_code` = 'BG' AND p.`elevation` > 2835
ORDER BY p.`elevation` DESC;


#Ex_13_Count Mountain Ranges
SELECT `country_code`, COUNT(m.`mountain_range`)
FROM `countries` AS c
JOIN `mountains_countries` AS mc
USING(`country_code`)
JOIN `mountains` AS m
ON mc.`mountain_id` = m.`id`
WHERE c.`country_code` IN ('US', 'RU', 'BG')
GROUP BY c.`country_code`
ORDER BY c.`country_code`;

SELECT mc.`country_code`, COUNT(m.`id`) AS `mountain_range`
FROM `mountains` AS m
JOIN `mountains_countries` AS mc
ON mc.`mountain_id` = m.`id`
WHERE mc.`country_code` IN ('US', 'RU', 'BG')
GROUP BY mc.`country_code`
ORDER BY `mountain_range` DESC;


#Ex_14_Countries with Rivers
SELECT c.`country_name`, r.`river_name`
FROM `countries` AS c
LEFT JOIN `countries_rivers` AS cr
USING (`country_code`)
LEFT JOIN `rivers` AS r
ON cr.`river_id` = r.`id`
WHERE c.`continent_code` IN('AF')
ORDER BY c.`country_name`
LIMIT 5;


#Ex_15_Continents and Currencies
SELECT `continent_code`, `currency_code`, COUNT(`country_name`) AS `currency_usage`
FROM `countries` AS c
GROUP BY `continent_code`, `currency_code`
HAVING `currency_usage` = 
	(SELECT COUNT(`country_code`) AS `cc`
    FROM `countries` AS c1
    WHERE c1.`continent_code` = c.`continent_code`
    GROUP BY `currency_code`
    ORDER BY `cc` DESC
    LIMIT 1
    ) AND `currency_usage` > 1
    ORDER BY `continent_code`, `currency_code`;


#Ex_16_Countries Without Any Mountains
SELECT COUNT(`country_code`) AS `country_count`
FROM `countries` AS c
LEFT JOIN `mountains_countries` AS mc
USING(`country_code`)
WHERE mc.`mountain_id` IS NULL;

SELECT COUNT(*) AS `country_count`
FROM `countries` AS c
WHERE c.`country_code` NOT IN 
(SELECT `country_code` FROM `mountains_countries`);


#Ex_17_Highest Peak and Longest River by Country
SELECT 
    c.`country_name`,
    MAX(p.`elevation`) AS `highest_peak_elevation`,
    MAX(r.`length`) AS `longest_river_length`
FROM
    `countries` AS c
        LEFT JOIN
    `mountains_countries` AS mc USING (`country_code`)
        LEFT JOIN
    `mountains` AS m ON mc.`mountain_id` = m.`id`
        LEFT JOIN
    `peaks` AS p ON m.`id` = p.`mountain_id`
        LEFT JOIN
    `countries_rivers` AS cr USING (`country_code`)
        LEFT JOIN
    `rivers` AS r ON cr.`river_id` = r.`id`
GROUP BY c.`country_name`
ORDER BY `highest_peak_elevation` DESC , `longest_river_length` DESC , c.`country_name`
LIMIT 5;