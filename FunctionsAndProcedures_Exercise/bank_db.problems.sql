#Ex_08_Find Full Name
DELIMITER &&
CREATE PROCEDURE usp_get_holders_full_name()
BEGIN
	SELECT CONCAT_WS(`first_name`, `last_name`)
    FROM `account_holders`;
END &&
CALL usp_get_holders_full_name;


#Ex_09_People with Balance Higher Than
DELIMITER &&
CREATE PROCEDURE `usp_get_holders_with_balance_higher_than`(input_number DECIMAL)
BEGIN
	SELECT ah.`first_name`, ah.`last_name`
    FROM `account_holders` AS ah
    RIGHT JOIN `accounts` AS a
    ON ah.`id` = a.`account_holder_id`
    GROUP BY ah.`id`
    HAVING SUM(a.`balance`) > input_number
    ORDER BY ah.`id`;
END &&


#Ex_10_Future Value Function
DELIMITER &&
CREATE FUNCTION `ufn_calculate_future_value`(initial_sum DECIMAL(18, 4), yearly_interest_rate DOUBLE, number_of_years INT) RETURNS decimal(18,4)
DETERMINISTIC
BEGIN
	RETURN (initial_sum * (POWER((1 + yearly_interest_rate), number_of_years)));
END &&


#Ex_11_Calculating Interest
DELIMITER &&
CREATE PROCEDURE `usp_calculate_future_value_for_account`(account_id INT, interest_rate DOUBLE(18, 4))
BEGIN
	SELECT a.`id`, ah.`first_name`, ah.`last_name`, a.`balance` AS `current_balance`, 
    ufn_calculate_future_value (a.`balance`, interest_rate, 5)	AS `balance_in_5_years`
	FROM `accounts` AS a
	JOIN `account_holders` AS ah
	ON a.`account_holder_id` = ah.`id`
    WHERE a.`id` = account_id;
END &&


#Ex_12_Deposit Money
DELIMITER &&
CREATE PROCEDURE usp_deposit_money (account_id INT, money_amount DECIMAL(19, 4))
BEGIN
	START TRANSACTION;
    IF (SELECT COUNT(*) FROM `accounts` WHERE `id` = account_id) = 0
        OR (money_amount <= 0)
		THEN ROLLBACK;
    ELSE
		UPDATE `accounts`
        SET `balance` = `balance` + money_amount
        WHERE `id` = account_id;
    END IF;
END &&
CALL usp_deposit_money (1, 10);


#Ex_13_Withdraw Money
DELIMITER &&
CREATE PROCEDURE usp_withdraw_money(account_id INT, money_amount DECIMAL(19, 4))
BEGIN
	START TRANSACTION;
    IF (SELECT COUNT(*) FROM `accounts` WHERE `id` = account_id) = 0
        OR (money_amount <= 0)
        OR ((SELECT `balance` FROM `accounts` WHERE `id` = account_id) < money_amount)
		THEN ROLLBACK;
ELSE
		UPDATE `accounts`
        SET `balance` = `balance` - money_amount
        WHERE `id` = account_id;
    END IF;
END &&
CALL usp_withdraw_money (1, 10);


#Ex_14_Money Transfer
DELIMITER &&
CREATE PROCEDURE usp_transfer_money(from_account_id INT, to_account_id INT, amount DECIMAL(19,4))
BEGIN
	START TRANSACTION;
    
END &&


#Ex_15_Log Accounts Trigger
CREATE TABLE `logs` (
	`log_id` INT PRIMARY KEY AUTO_INCREMENT,
    `account_id` INT, 
    `old_sum` DECIMAL(19, 4), 
    `new_sum` DECIMAL(19, 4)
);

DELIMITER &&
CREATE TRIGGER tr_update_accounts
AFTER UPDATE
ON `accounts`
FOR EACH ROW
BEGIN
	INSERT INTO `logs` (`account_id`, `old_sum`, `new_sum`)
    VALUES (OLD.id, OLD.balance, NEW.balance);
END &&


#Ex_16_Emails Trigger
CREATE TABLE `notification_emails` (
	`id` INT PRIMARY KEY AUTO_INCREMENT, 
    `recipient` INT, 
    `subject` VARCHAR(100), 
    `body` VARCHAR(300)
);

DELIMITER &&
CREATE TRIGGER tr_new_email_logs	
AFTER INSERT
ON `logs`
FOR EACH ROW
BEGIN
	INSERT INTO `notification_emails` (`recipient`, `subject`, `body`)
    VALUES (NEW.account_id, 
			CONCAT('Balance change for account: ', NEW.account_id), 
			CONCAT('On ', DATE_FORMAT(NOW(), '%b %d %Y at %r'), ' your balance was changed from ', 
				ROUND(NEW.old_sum, 2), 
				' to ', 
				ROUND(NEW.new_sum, 2)
				'.')
	);
END &&