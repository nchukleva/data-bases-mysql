#Ex_01_Employees with Salary Above 35000
DELIMITER &&
CREATE PROCEDURE usp_get_employees_salary_above_35000()
BEGIN

	SELECT `first_name`, `last_name`
    FROM `employees`
    WHERE `salary` > 35000
    ORDER BY `first_name`, `last_name`, `employee_id`;
    
END &&
CALL usp_get_employees_salary_above_35000;


#Ex_02_Employees with Salary Above Number
DELIMITER &&
CREATE PROCEDURE usp_get_employees_salary_above (salary_rate DECIMAL(19, 4))
BEGIN
	SELECT `first_name`, `last_name`
    FROM `employees`
    WHERE `salary` >= salary_rate
    ORDER BY `first_name`, `last_name`, `employee_id`;

END &&
CALL usp_get_employees_salary_above(45000);


#Ex_03_Town Names Starting With
DELIMITER &&
CREATE PROCEDURE usp_get_towns_starting_with (starting_with VARCHAR(20))
BEGIN
	SELECT `name` AS `town_name`
	FROM `towns`
	WHERE `name` LIKE CONCAT(starting_with, '%')
    ORDER BY `town_name`;
END &&
CALL usp_get_towns_starting_with('b');


#Ex_04_Employees from Town
DELIMITER &&
CREATE PROCEDURE usp_get_employees_from_town (town_name VARCHAR(50))
BEGIN
SELECT `first_name`, `last_name` 
	FROM `employees` AS e
	JOIN `addresses` AS a
	USING (`address_id`)
	JOIN `towns` AS t
	USING (`town_id`)
	WHERE t.`name` = town_name
	ORDER BY `first_name`, `last_name`, `employee_id`;
END &&
CALL usp_get_employees_from_town('Sofia');


#Ex_05_Salary Level Function
DELIMITER &&
CREATE FUNCTION ufn_get_salary_level (employee_salary DECIMAL)
RETURNS VARCHAR(7)
DETERMINISTIC
BEGIN
	RETURN (CASE
		WHEN(employee_salary < 30000) THEN 'Low'
		WHEN(employee_salary BETWEEN 30000 AND 50000) THEN 'Average'
        WHEN(employee_salary > 50000) THEN 'High'
		END 
	);

END &&
SELECT ufn_get_salary_level(125500);

/*CREATE FUNCTION ufn_get_salary_level (employee_salary DECIMAL)
RETURNS VARCHAR(7)
DETERMINISTIC
RETURN (CASE
			WHEN(employee_salary < 30000) THEN 'Low'
			WHEN(employee_salary < 50000) THEN 'Average'
			ELSE 'High'
            END);*/


#Ex_06_Employees by Salary Level
DELIMITER &&
CREATE PROCEDURE usp_get_employees_by_salary_level (salary_level VARCHAR(7))
BEGIN
	SELECT `first_name`, `last_name` 
	FROM `employees`
    WHERE ufn_get_salary_level(`salary`) = salary_level
    ORDER BY `first_name` DESC, `last_name` DESC;
	
END &&
CALL usp_get_employees_by_salary_level('high');


#Ex_07_Define Function
DELIMITER &&
CREATE FUNCTION ufn_is_word_comprised(set_of_letters varchar(50), word varchar(50))
RETURNS BIT
DETERMINISTIC
BEGIN
	RETURN word REGEXP(CONCAT('^[', set_of_letters, ']+$'));
END &&

	



