SELECT * FROM `employees`;

#1
SELECT `id`, `first_name`, `last_name`, `job_title` FROM `employees`
ORDER BY `id`;

#2    
SELECT `id`, concat(`first_name`,' ',`last_name`) AS 'Full name', 
	`job_title` AS 'Job title', 
	`salary` AS 'Salary'
  FROM `employees` WHERE salary > 1000
  ORDER BY `id`;
  
#3
SELECT * FROM `employees`;

UPDATE `employees`
SET `salary` = `salary` + 100
WHERE `job_title` = 'Manager';

SELECT `salary` FROM `employees` ORDER BY `salary` DESC;

#5  
SELECT `id`, `first_name`, `last_name`, `job_title`, `department_id`, `salary` FROM `employees`
	WHERE `department_id` = 4 AND `salary` >= 1000
    ORDER BY `id`;

#
SELECT * FROM `employees`
WHERE `job_title` != 'Manager'
ORDER BY `salary` DESC;

#create virtual table
CREATE VIEW `employees_name_salary` AS
SELECT `id`, concat_ws(' ', `first_name`, `last_name`) AS `full_name`, `salary` 
FROM `employees`;

#4
CREATE VIEW `v_top_paid_employee` AS
SELECT * FROM `employees`
ORDER BY `salary` DESC
LIMIT 1;

SELECT * FROM `v_top_paid_employee`;

SELECT * FROM `employees`;
CREATE TABLE `employee_salary` AS
SELECT `id`, concat_ws(' ', `first_name`, `last_name`) AS `full_name`, `salary`
FROM `employees`
ORDER BY `salary` DESC;

SELECT * FROM `employee_salary`;

#
CREATE TABLE `projects` (
`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
`name` VARCHAR(50) NOT NULL,
`start_date` DATE,
`manager_id` INT NOT NULL,
CONSTRAINT `fk_manager_id`
FOREIGN KEY (`manager_id`) REFERENCES `employees`(`id`)
);

INSERT INTO `projects` (`name`, `start_date`, `manager_id`)
SELECT CONCAT(`name`, ' Restructuring'), NOW(), 1 
FROM `departments`;

#6
DELETE FROM `employees` 
WHERE `department_id` IN (1, 2);

SELECT * FROM `employees` ORDER BY `id`;
